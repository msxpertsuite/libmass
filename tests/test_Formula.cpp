// tests/catch2-tests [section] -s

#include <QDebug>
#include <QString>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>

TEST_CASE("Formula test suite", "[Formula]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: Formula initialization ::..", "[Formula]")
  {
    bool ok = true;
    REQUIRE(ok == true);
  }
}

