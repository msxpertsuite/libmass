# Packager

# Listed below is the doc/user-manual/DC-user-manual:
# this is a generated file (configure_file) that needs not be
# in the tarball. However, because it is a configure_file it is 
# generated right while doing 'cmake archive', so it is not
# possible that it be absent from source dir! Which is why
# it is listed here.

set(CPACK_PACKAGE_NAME "minexpert2")
set(CPACK_PACKAGE_VENDOR "msXpertSuite")
set(CPACK_PACKAGE_VERSION "${VERSION}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Software for visualization and exploration of mass spectrometric data")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE")
set(CPACK_RESOURCE_FILE_AUTHORS "${CMAKE_SOURCE_DIR}/AUTHORS")
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${LOWCASE_PROJECT_NAME}-${CPACK_PACKAGE_VERSION}")

set(CPACK_SOURCE_IGNORE_FILES
  .cache/
  \\.git/
  /\\.kdev4/
  maintainer-scripts/
  .*kdev.*
  compile_commands.json
  TODO
  doc/user-manual/DC-user-manual
  libmass.a
  libmassgui.a
  minexpert2-doc.pdf)

set(CPACK_VERBATIM_VARIABLES YES)
include(CPack)


