/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// IsoSpec
#include <IsoSpec++/isoSpec++.h>
#include <IsoSpec++/element_tables.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "PeakCentroid.hpp"
#include "IsotopicDataUserConfigHandler.hpp"


namespace msxps
{

namespace libmass
{


IsotopicDataUserConfigHandler::IsotopicDataUserConfigHandler(
  const QString &file_name)
  : IsotopicDataBaseHandler(file_name)
{
}


IsotopicDataUserConfigHandler::IsotopicDataUserConfigHandler(
  IsotopicDataSPtr isotopic_data_sp, const QString &file_name)
  : IsotopicDataBaseHandler(isotopic_data_sp, file_name)
{
}


IsotopicDataUserConfigHandler::~IsotopicDataUserConfigHandler()
{
  // qDebug();
}


std::size_t
IsotopicDataUserConfigHandler::loadData(const QString &file_name)
{
  // qDebug() << "Loading isotopic data from file:" << file_name;

  // The format of the file from which data is to be loaded is mimicking the
  // tables that are defined as a number of C arrays in the libIsoSpec++
  // header element_tables.c/h files.
  //
  // See the Isotope::toString() function that is used to write the isotopic
  // data to file. We thus expect exactly that format from the file.

  if(file_name.isEmpty())
    {
      qDebug("File name is emtpy. Failed to open file for reading.");
      return 0;
    }

  QFile file(file_name);

  if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      qDebug("Failed to open file for reading.");
      return false;
    }

  // We may have written comment to the file in the form of #<text> lines
  QRegularExpression commentRegexp("^\\s*#.*$");

  msp_isotopicData->clear();

  std::size_t isotope_count = 0;

  QTextStream in(&file);

  while(!in.atEnd())
    {
      QString line = in.readLine().simplified();

      // qDebug() << "simplified line:" << line;

      // Ignore empty or comment lines
      if(line.length() < 1 || commentRegexp.match(line).hasMatch())
        continue;

      IsotopeSPtr isotope_sp = std::make_shared<Isotope>(line);

      // We do not want to update the mono/avg maps each time we load an
      // isotope. We'll call the relevant function later.
      msp_isotopicData->appendNewIsotope(isotope_sp, false);

      ++isotope_count;
    }
  // End of
  // while(!in.atEnd())

  // qDebug() << "Finished creating all the Isotope instances.";

  file.close();

  // At this point, it seems that the loading went fine.

  // Because we have touched the m_isotopes vector, we need to update the
  // mono/avg masses map.

  if(!msp_isotopicData->updateMassMaps())
    qFatal("Programming error. Failed to update the mass maps.");

  if(isotope_count != msp_isotopicData->size())
    qFatal("Programming error. Failed to load all the isotopes to file.");

  return msp_isotopicData->size();
}


std::size_t
IsotopicDataUserConfigHandler::writeData(const QString &file_name)
{
  // Although the isotopic data were loaded from the IsoSpec library tables, we
  // might be willing to store these data to a file.

  if(file_name.isEmpty())
    {
      // qDebug() << "The passed file name is empty. Trying the member datum.";

      if(m_fileName.isEmpty())
        {
          qDebug() << "The member datum also is empty. Cannot do anything.";

          return 0;
        }
    }
  else
    {
      // The passed filename takes precedence over the member datum. So copy
      // that file name to the member datum.

      m_fileName = file_name;
    }

  QFile file(m_fileName);

  if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
      qDebug("Failed to open file for writing.");
      return false;
    }

  QTextStream out(&file);

  out << "# This file contains isotopic data in a format that can accommodate\n";
  out
    << "# comments in the form of lines beginning with the '#' character.\n\n";

  std::size_t isotope_count = 0;

  for(auto item : msp_isotopicData->m_isotopes)
    {
      out << item->toString();
      // We need to add it because toString() does not terminate the line with
      // a new line character.
      out << "\n";

      ++isotope_count;
    }

  out.flush();

  file.close();

  if(isotope_count != msp_isotopicData->size())
    qFatal("Programming error. Failed to write all the isotopes to file.");

  return isotope_count;
}


std::size_t
IsotopicDataUserConfigHandler::checkConsistency()
{
  return msp_isotopicData->size();
}


} // namespace libmass

} // namespace msxps


#if 0

Example from IsoSpec.

const int elementNumber = 2;
const int isotopeNumbers[2] = {2,3};

const int atomCounts[2] = {2,1};


const double hydrogen_masses[2] = {1.00782503207, 2.0141017778};
const double oxygen_masses[3] = {15.99491461956, 16.99913170, 17.9991610};

const double* isotope_masses[2] = {hydrogen_masses, oxygen_masses};

const double hydrogen_probs[2] = {0.5, 0.5};
const double oxygen_probs[3] = {0.5, 0.3, 0.2};

const double* probs[2] = {hydrogen_probs, oxygen_probs};

IsoLayeredGenerator iso(Iso(elementNumber, isotopeNumbers, atomCounts, isotope_masses, probs), 0.99);

#endif
