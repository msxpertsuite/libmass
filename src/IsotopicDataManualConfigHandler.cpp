/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Std lib includes
#include <set>
#include <cassert>


/////////////////////// Qt includes
#include <QDebug>
#include <QFile>
#include <QIODevice>


/////////////////////// IsoSpec
#include <IsoSpec++/isoSpec++.h>
#include <IsoSpec++/element_tables.h>


/////////////////////// Local includes
#include "IsotopicDataManualConfigHandler.hpp"


namespace msxps
{

namespace libmass
{


IsotopicDataManualConfigHandler::IsotopicDataManualConfigHandler(
  const QString &file_name)
  : IsotopicDataBaseHandler(file_name)
{
}


IsotopicDataManualConfigHandler::IsotopicDataManualConfigHandler(
  IsotopicDataSPtr isotopic_data_sp, const QString &file_name)
  : IsotopicDataBaseHandler(isotopic_data_sp, file_name)
{
}


IsotopicDataManualConfigHandler::~IsotopicDataManualConfigHandler()
{
  // qDebug();
}


void
IsotopicDataManualConfigHandler::setSymbolCountMap(const SymbolCountMap &map)
{
  m_symbolCountMap = map;
}


const SymbolCountMap &
IsotopicDataManualConfigHandler::getSymbolCountMap() const
{
  return m_symbolCountMap;
}


std::size_t
IsotopicDataManualConfigHandler::loadData(const QString &file_name)
{
  // File format:
  //
  // [Element]
  // 	symbol C count 6
  // 	[Isotopes] 2
  // 		mass 12.0 prob 0.989
  // 		mass 13.003354 prob 0.010788
  // [Element]
  // 	symbol H count 13
  // 	[Isotopes] 2
  // 		mass 1.0078250 prob 0.99988
  // 		mass 2.01410177 prob 0.00011570
  // [Element]
  // 	symbol O count 6
  // 	[Isotopes] 3
  // 		mass 15.9949 prob 0.99756
  // 		mass 16.999 prob 0.000380
  // 		mass 17.999 prob 0.002051
  //

  if(file_name.isEmpty())
    {
      qDebug("File name is emtpy. Failed to open file for reading.");
      return 0;
    }

  QFile file(file_name);

  if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      qDebug() << "Failed to open file for reading.";
      return 0;
    }

  // File-parsing helper variables.
  bool was_started_one_element = false;
  bool was_symbol_count_line   = false;
  bool was_isotopes_count_line = false;
  bool was_mass_prob_line      = false;

  // Instantiate a symbol that we'll use as a place holder for the element name.
  QString symbol = "";

  // Instantiate a count to document the symbol count.
  std::size_t element_count = 0;

  // Instantiate a count to document the number of isotopes that were defined
  // for a given element.
  std::size_t element_isotope_count = 0;

  // Increment each time an isotope is parsed and added to the vector *for a
  // given element stanza*. This helps sanity checking.
  std::size_t added_isotopes_for_element = 0;

  // Maintain a counter of the whole count of isotopes for sanity checks.
  std::size_t all_parsed_isotopes_count = 0;

  double mass = 0;
  double prob = 0;

  bool ok = false;

  QRegularExpression comment_regexp("^\\s*#");

  QRegularExpression symbol_count_regexp(
    "^\\s*symbol\\s+([A-Z][a-z]?)\\s+count\\s+(\\d+)");

  QRegularExpression isotopes_regexp("^\\s*\\[Isotopes\\]\\s(\\d+)");

  // QRegularExpression massProbRegexp = QRegularExpression(
  //"^\\s+mass\\s+(\\d*\\.?\\d*[e]?[-]?[+]?\\d*)\\s+prob\\s+([^\\d^\\.^-]+)(-?"
  //"\\d*\\.?\\d*[e]?[-]?[+]?\\d*)");

  QRegularExpression mass_prob_regexp = QRegularExpression(
    "^\\s*mass\\s(\\d*\\.?\\d*[e]?[-]?[+]?\\d*)\\sprob\\s(\\d*\\.?\\d*[e]?["
    "-]"
    "?["
    "+]?\\d*)");

  // qDebug() << "The mass prob regexp is valid?" <<
  // massProbRegexp.isValid();

  // mass 13.003354835200
  // prob 0.010788058149533083507343178553128382191061973571777343750000

  // Make sure we clear the room.

  msp_isotopicData->clear();

  std::vector<IsotopeSPtr> element_isotopes;

  // This set is to ensure that we do not have twice the same element frame
  // (that is, with the same symbol).

  std::set<QString> symbol_set;

  // File format:

  // [Element]
  // 	symbol C count 6
  // 	[Isotopes] 2
  // 		mass 12.0 prob 0.989
  // 		mass 13.003354 prob 0.010788

  QTextStream in(&file);

  while(!in.atEnd())
    {
      QString line = in.readLine();

      // Ignore empty lines
      if(line.length() < 1)
        continue;


      line = line.simplified();

      // qDebug() << "Current  line:" << line;

      // Ignore comment lines
      QRegularExpressionMatch match = comment_regexp.match(line);
      if(match.hasMatch())
        continue;

      if(line == "[Element]")
        {
          // qDebug() << "That's the [Element] stanza opening line.";

          // We are starting a new Element stanza. It cannot be that we both
          // have already started one Element stanza and that not a single
          // mass and probability line had been encountered. Either this is the
          // very first Element stanza that we read and was_started_one_element
          // is false or we were reading one Element stanza that has finished
          // and then was_mass_prob_line has to be true.
          if(was_started_one_element && !was_mass_prob_line)
            {
              qDebug() << "Error: one element is complete but has no isotopes.";
              return 0;
            }

          if(was_started_one_element)
            {

              // qDebug()
              //<< "We had already seen the [Element] stanza opening line.";

              // We are starting a new Element configuration stanza, but in
              // fact another was already cooking. We need to terminate it.

              // Sanity check: the number of purportedly listed isotopes needs
              // to be identical to the number of isotopes actually added to the
              // vector of isotopes.

              if(element_isotope_count != element_isotopes.size() ||
                 added_isotopes_for_element != element_isotopes.size())
                {
                  qDebug() << "Error. We did not parse the expected number of "
                              "isotopes.";
                  return 0;
                }

              // qDebug() << "And there are the right number of isotopes
              // cooked.";

              // At this point we have everything we need to add this new
              // chemical set.

              // qDebug() << "Creating new chemical set.";

              if(!newChemicalSet(
                   symbol, element_count, element_isotopes, false))
                {
                  qDebug() << "Failed to add new chemical set.";
                  return 0;
                }

              // qDebug() << "The completed chemical set: "
              //"symbol/element_count/isotope_count:"
              //<< symbol << "/" << element_count << "/"
              //<< element_isotopes.size();

              // Sanity check: the total count of added isotopes for this symbol
              // needs to correct.

              if(added_isotopes_for_element !=
                 msp_isotopicData->getIsotopeCountBySymbol(symbol))
                qFatal("Programming error.");

              // Now clear for next run.
              symbol                     = "";
              element_isotope_count      = 0;
              added_isotopes_for_element = 0;
              element_isotopes.clear();
            }

          // Tell that we actually have entered the first line of an Element
          // stanza.
          was_started_one_element = true;

          // Reset all the other values so that we know we are just at the
          // beginning of the parsing work.
          was_symbol_count_line   = false;
          was_isotopes_count_line = false;
          was_mass_prob_line      = false;

          // Go the next line.
          continue;
        }

      // At this point we are already inside of the [Element] stanza. Parse the
      // various lines inside it.

      // File format:

      // [Element]
      // 	symbol C count 6
      // 	[Isotopes] 2
      // 		mass 12.0 prob 0.989
      // 		mass 13.003354 prob 0.010788

      match = symbol_count_regexp.match(line);
      if(match.hasMatch())
        {

          // qDebug() << "Matched the symbol count line.";

          // If we are parsing "symbol C count 100", then it is not possible
          // that we did not encounter before [Element] or that we already have
          // parsed one same line as "symbol C count 100".

          if(!was_started_one_element || was_symbol_count_line)
            {
              qDebug() << "Error encountered in the symbol/count line.";
              return 0;
            }

          symbol = match.captured(1);

          element_count = match.captured(2).toInt(&ok);
          if(!ok || !element_count)
            {
              qDebug() << "Error encountered in the symbol/count line.";
              return 0;
            }

          // Now check if that symbol was encountered already, which would be an
          // error.
          auto res = symbol_set.insert(symbol);
          if(!res.second)
            {
              // We did not insert the symbol because one already existed. That
              // is an error.

              qDebug() << "An element by symbol" << symbol
                       << "has already been processed: "
                          "this is not permitted.";
              return 0;
            }

          // qDebug() << "Processed element symbol:" << symbol
          //<< "with count:" << element_count;

          // Do not store the element symbol/count pair yet, we'll wait to
          // encounter a new Element stanza which will close this one.

          was_symbol_count_line   = true;
          was_isotopes_count_line = false;
          was_mass_prob_line      = false;

          continue;
        }
      // End of
      // line matched the symbol/count regexp

      // File format:

      // [Element]
      // 	symbol C count 6
      // 	[Isotopes] 2
      // 		mass 12.0 prob 0.989
      // 		mass 13.003354 prob 0.010788

      match = isotopes_regexp.match(line);
      if(match.hasMatch())
        {

          // qDebug() << "Matched the [Isotopes] count stanza opening line.";

          // We cannot be parsing the [Isotopes] stanza opening header if we
          // have not previously parsed the symbol/count line.
          if(!was_symbol_count_line)
            {
              qDebug() << "Error encounteredd in the isotopes lines.";
              return 0;
            }

          // Store the number of isotopes for the current Element.
          element_isotope_count = match.captured(1).toInt(&ok);
          if(!ok || !element_isotope_count)
            {
              qDebug() << "Error encounteredd in the isotopes lines.";
              return 0;
            }

          // qDebug() << "The isotope count is:" << element_isotope_count;

          was_isotopes_count_line = true;
          was_symbol_count_line   = false;
          was_mass_prob_line      = false;

          continue;
        }
      // End of
      // line matched the [Isotopes] count regexp

      // File format:

      // [Element]
      // 	symbol C count 6
      // 	[Isotopes] 2
      // 		mass 12.0 prob 0.989
      // 		mass 13.003354 prob 0.010788

      match = mass_prob_regexp.match(line);
      if(match.hasMatch())
        {

          // qDebug() << "Matched the mass prob line.";

          // If we match an isotope's mass/prob line, either --- at previous
          // line --- we had seen the [Isotopes] stanza opening line or we had
          // seen another mass prob line.


          if(!was_isotopes_count_line && !was_mass_prob_line)
            {
              qDebug() << "Error encountered in the mass/prob line.";
              return 0;
            }

          mass = match.captured(1).toDouble(&ok);
          if(!ok)
            {
              qDebug() << "Error encountered in the mass/prob line.";
              return 0;
            }

          prob = match.captured(2).toDouble(&ok);
          if(!ok)
            {
              qDebug() << "Error encountered in the mass/prob line.";
              return 0;
            }

          // At this point we have everything we need to actually create the
          // isotope by symbol and mass and prob. There are a number of fields
          // that are left to value 0 but this is of no worries.

          // qDebug() << "Iterated in isotope:" << symbol << ":" << mass << "/"
          //<< prob;

          // At this point create a brand new Isotope with the relevant data.

          // Isotope::Isotope(int id,
          // QString element,
          // QString symbol,
          // int atomicNo,
          // double mass,
          // int massNo,
          // int extraNeutrons,
          // double probability,
          // double logProbability,
          // bool radioactive)

          // There are a number of fields that are left to value 0 but this is
          // of no worries. Store the isotope in the vector of isotopes that
          // will be added to the isotopic data later when finishing the parsing
          // of the element frame widget.

          element_isotopes.push_back(std::make_shared<libmass::Isotope>(
            0, symbol, symbol, 0, mass, 0, 0, prob, 0, false));

          ++added_isotopes_for_element;
          ++all_parsed_isotopes_count;

          // qDebug() << "The atom now is:" << atom.asText();

          was_mass_prob_line      = true;
          was_isotopes_count_line = false;
          was_symbol_count_line   = false;

          continue;
        }
      // End of
      // line matched the isotope mass/prob regexp
    }


  // We have finished iterating in the file's lines but we were parsing an atom,
  // append it.

  // Sanity check
  if(!was_started_one_element)
    {
      qDebug() << "Error: not a single element could be parsed.";
      return 0;
    }

  // Sanity check: the number of purportedly listed isotopes needs
  // to be identical to the number of isotopes actually added to the
  // vector of isotopes.

  if(element_isotope_count != element_isotopes.size() ||
     added_isotopes_for_element != element_isotopes.size())
    {
      qDebug() << "Error. We did not parse the expected number of "
                  "isotopes.";
      return 0;
    }

  // At this point we have everything we need to add this new
  // chemical set. Do not yet update the mass maps, we'll do at the end of the
  // process.

  if(!newChemicalSet(symbol, element_count, element_isotopes, false))
    {
      qDebug() << "Failed to add new chemical set.";
      return 0;
    }

  // qDebug() << "The completed chemical set: "
  //"symbol/element_count/isotope_count:"
  //<< symbol << "/" << element_count << "/" << element_isotopes.size();

  // Sanity check: the total count of added isotopes for this symbol
  // needs to correct.

  if(added_isotopes_for_element !=
     msp_isotopicData->getIsotopeCountBySymbol(symbol))
    qFatal("Programming error.");

  // Sanity check: the total count of isotopes added to the isotopic data member
  // datum needs to match the total count of parsed isotopes.
  if(all_parsed_isotopes_count != msp_isotopicData->size())
    qFatal("Programming error.");

  // We have touched the isotopic data, ensure the maps are current.
  if(!msp_isotopicData->updateMassMaps())
    qFatal("Programming error. Failed to update the mass maps.");

  return msp_isotopicData->size();
}


std::size_t
IsotopicDataManualConfigHandler::writeData(const QString &file_name)
{
  // Although the isotopic data were loaded from the IsoSpec library tables, we
  // might be willing to store these data to a file.

  if(file_name.isEmpty())
    {
      qDebug() << "The passed file name is empty. Trying the member datum.";

      if(m_fileName.isEmpty())
        {
          qDebug() << "The member datum also is empty. Cannot do anything.";

          return 0;
        }
    }
  else
    {
      // The passed filename takes precedence over the member datum. So copy
      // that file name to the member datum.

      m_fileName = file_name;
    }

  QFile file(m_fileName);

  if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
      qDebug("Failed to open file for writing.");
      return false;
    }

  QTextStream out(&file);

  out << "# This file contains isotopic data in a format that can accommodate\n";
  out << "# comments in the form of lines beginning with the '#' character.\n";

  std::size_t isotope_count = 0;

  // We want to write the isotopic data exactly in the same order as we might
  // have loaded them. This is why we need to iterated in the vector of isotopes
  // and not the in the map that is ordered according to the symbols (the keys).

  QString last_symbol;

  for(auto item : msp_isotopicData->m_isotopes)
    {
      QString symbol = item->getSymbol();

      // We only write the  [Element] and [Isotopes] stanza header once for each
      // new symbol found in the iterated items.

      if(symbol != last_symbol)
        {

          // This is the first time we encounter an isotope by symbol, we open a
          // new Element stanza.
          out << "[Element]\n";

          // We now need to write the symbol count line. If the symbol key is
          // not found, throws an exception.
          int symbol_count = m_symbolCountMap.at(symbol);
          out
            << QString("\tsymbol %1 count %2\n").arg(symbol).arg(symbol_count);

          // We also need to write the Isotopes stanza:
          int isotope_count = msp_isotopicData->getIsotopeCountBySymbol(symbol);
          out << QString("\t[Isotopes] %1\n").arg(isotope_count);
        }

      // At this point we can write the currently iterated isotope's mass:prob
      // pair.

      out << QString("\t\tmass %1 prob %2\n")
               .arg(item->getMass(), 0, 'f', 60)
               .arg(item->getProbability(), 0, 'f', 60);

      last_symbol = symbol;

      ++isotope_count;
    }

  out.flush();

  file.close();

  if(isotope_count != msp_isotopicData->size())
    qFatal("Programming error. Failed to write all the isotopes to file.");

  return true;
}


bool
IsotopicDataManualConfigHandler::newChemicalSet(
  const QString &symbol,
  int element_count,
  const std::vector<IsotopeSPtr> &isotopes,
  bool update_maps)
{
  // It is of uttermost importance that we update the symbol/count pair because
  // that is going to be used when performing the IsoSpec arrays configuration
  // and later isotopic cluster calculations.

  std::pair<SymbolCountMapIter, bool> res =
    m_symbolCountMap.insert(std::pair<QString, int>(symbol, element_count));

  if(!res.second)
    {
      // Sanity check: it is not possible to add new chemical sets by a given
      // symbol multiple times.

      qDebug() << "Error: isotopes by that symbol: " << symbol
               << "were already found in the isotopic data set.";

      return false;
    }

  std::size_t count_before = msp_isotopicData->size();

  // Update the mass maps according to second param below.
  msp_isotopicData->appendNewIsotopes(isotopes, update_maps);

  std::size_t count_after = msp_isotopicData->size();

  if(count_after - count_before != isotopes.size())
    qFatal("Programming error.");

  return true;
}


QString
IsotopicDataManualConfigHandler::craftFormula() const
{
  QString formula;

  for(auto item : m_symbolCountMap)
    formula.append(QString("%1%2").arg(item.first).arg(item.second));

  return formula;
}


std::size_t
IsotopicDataManualConfigHandler::checkConsistency()
{
  return msp_isotopicData->size();
}


} // namespace libmass

} // namespace msxps


#if 0

Example from IsoSpec.

const int elementNumber = 2;
const int isotopeNumbers[2] = {2,3};

const int atomCounts[2] = {2,1};


const double hydrogen_masses[2] = {1.00782503207, 2.0141017778};
const double oxygen_masses[3] = {15.99491461956, 16.99913170, 17.9991610};

const double* isotope_masses[2] = {hydrogen_masses, oxygen_masses};

const double hydrogen_probs[2] = {0.5, 0.5};
const double oxygen_probs[3] = {0.5, 0.3, 0.2};

const double* probs[2] = {hydrogen_probs, oxygen_probs};

IsoLayeredGenerator iso(Iso(elementNumber, isotopeNumbers, atomCounts, isotope_masses, probs), 0.99);

#endif
