/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "FragRule.hpp"
#include "PolChemDef.hpp"


namespace msxps
{

namespace libmass
{


//! Constructs a fragmentation rule.
/*!

  \param polChemDef Polymer chemistry definition. Cannot be 0.

  \param name Name. Cannot be empty.

  \param prevCode Previous monomer code. Defaults to the null string.

  \param currCode  Current monomer code. Defaults to the null string.

  \param nextCode Next monomer code. Defaults to the null string.

  \param formula Formula. Defaults to the null string.

  \param comment Comment. Defaults to the null string.
*/
FragRule::FragRule(PolChemDefCstSPtr polChemDefCstSPtr,
                   QString name,
                   QString prevCode,
                   QString currCode,
                   QString nextCode,
                   QString formula,
                   const QString &comment)
  : PolChemDefEntity(polChemDefCstSPtr, name),
    Formula(formula),
    m_prevCode(prevCode),
    m_currCode(currCode),
    m_nextCode(nextCode),
    m_comment(comment)
{
}


//! Constructs a copy of \p other.
/*!  \param other fragmentation rule to be used as a mold.
 */
FragRule::FragRule(const FragRule &other)
  : PolChemDefEntity(other),
    Formula(other),
    m_prevCode(other.m_prevCode),
    m_currCode(other.m_currCode),
    m_nextCode(other.m_nextCode),
    m_comment(other.m_comment)
{
}


//! Destroys the fragmentation rule.
FragRule::~FragRule()
{
}


//! Assigns other to \p this fragmentation rule.
/*! \param other fragmentation rule used as the mold to set values to \p this
  instance.

  \return a reference to \p this fragmentation rule.
*/
FragRule &
FragRule::operator=(const FragRule &other)
{
  if(&other == this)
    return *this;

  PolChemDefEntity::operator=(other);
  Formula::operator         =(other);

  m_prevCode = other.m_prevCode;
  m_currCode = other.m_currCode;
  m_nextCode = other.m_nextCode;
  m_comment  = other.m_comment;

  return *this;
}


//! Sets the previous monomer code.
/*! \param str new code.
 */
void
FragRule::setPrevCode(const QString &str)
{
  m_prevCode = str;
}


//! Returns the previous monomer code.
/*! \return the code.
 */
QString
FragRule::prevCode() const
{
  return m_prevCode;
}


//! Sets the current monomer code.
/*! \param str new code.
 */
void
FragRule::setCurrCode(const QString &str)
{
  m_currCode = str;
}


//! Returns the current monomer code.
/*! \return the code.
 */
QString
FragRule::currCode() const
{
  return m_currCode;
}


//! Sets the next monomer code.
/*! \param str new code.
 */
void
FragRule::setNextCode(const QString &str)
{
  m_nextCode = str;
}


//! Returns the next monomer code.
/*! \return the code.
 */
QString
FragRule::nextCode() const
{
  return m_nextCode;
}


//! Sets the comment.
/*! \param str new comment.
 */
void
FragRule::setComment(const QString &str)
{
  m_comment = str;
}


//! Returns the comment.
/*! \return the comment.
 */
QString
FragRule::comment() const
{
  return m_comment;
}


QString
FragRule::formula() const
{
  return Formula::toString();
}


//! Searches a fragmentation rule in a list.
/*! Searches for a fragmentation rule having a name identical to
  argument \p str in \p refList. If such fragmentation rule is found,
  and if \p other is non-0, the found fragmentation rule's data are
  copied into \p other.

  \param str name.

  \param refList list of fragmentation rules.

  \param other fragmentation rule to be updated with data in the found
  fragmentation rule.  Defaults to 0, in which case no update occurs.

  \return the int index of the found fragmentation rule or -1 if none
  is found or if \p str is empty.
*/
int
FragRule::isNameInList(const QString &str,
                       const QList<FragRule *> &refList,
                       FragRule *other)
{
  FragRule *fragRule = 0;

  if(str.isEmpty())
    return -1;

  for(int iter = 0; iter < refList.size(); ++iter)
    {
      fragRule = refList.at(iter);
      Q_ASSERT(fragRule);

      if(fragRule->m_name == str)
        {
          if(other)
            *other = *fragRule;

          return iter;
        }
    }

  return -1;
}


//! Validates the fragmentation rule.
/*!
  \return true upon success, false otherwise.
*/
bool
FragRule::validate()
{
  const QList<Monomer *> &monomerRefList = mcsp_polChemDef->monomerList();

  if(m_name.isEmpty())
    return false;

  if(!m_prevCode.isEmpty())
    if(Monomer::isCodeInList(m_prevCode, monomerRefList) == -1)
      return false;

  if(!m_currCode.isEmpty())
    if(Monomer::isCodeInList(m_currCode, monomerRefList) == -1)
      return false;

  if(!m_nextCode.isEmpty())
    if(Monomer::isCodeInList(m_nextCode, monomerRefList) == -1)
      return false;

  IsotopicDataCstSPtr isotopic_data_csp =
    mcsp_polChemDef->getIsotopicDataCstSPtr();

  return Formula::validate(isotopic_data_csp);

  return true;
}


//! Parses a fragmentation rule XML element.
/*! Parses the fragmentation specification XML element passed as
  argument and sets its data to \p this fragmentation rule(this is
  called XML rendering).

  \param element XML element to be parsed and rendered.

  \return true if parsing and formula validation were successful,
  false otherwise.

  \sa formatXmlFgrElement(int offset, const QString &indent).
*/
bool
FragRule::renderXmlFgrElement(const QDomElement &element)
{
  QDomElement child;

  bool prevSet    = false;
  bool currSet    = false;
  bool nextSet    = false;
  bool commentSet = false;

  /* The xml node we are in is structured this way:
   *
   * <fgr>
   *   <name>one_rule</name>
   *   <formula>+H2O</formula>
   *   <prev-mnm-code>M</prev-mnm-code>
   *   <this-mnm-code>Y</this-mnm-code>
   *   <next-mnm-code>T</next-mnm-code>
   *   <comment>opt_comment</comment>
   * </fgr>
   *
   * And the element parameter points to the
   *
   * <fgr> element tag:
   *  ^
   *  |
   *  +----- here we are right now.
   *
   * Which means that element.tagName() == "fgr" and that
   * we'll have to go one step down to the first child of the
   * current node in order to get to the <name> element.
   *
   * The DTD:
   *    <!ELEMENT fgr(name,formula,prev-mnm-code?,
   *                   this-mnm-code?,next-mnm-code?,comment?)>
   */

  if(element.tagName() != "fgr")
    return false;

  child = element.firstChildElement();

  if(child.isNull() || child.tagName() != "name")
    return false;

  m_name = child.text();

  child = child.nextSiblingElement();

  if(child.isNull() || child.tagName() != "formula")
    return false;

  if(!Formula::renderXmlFormulaElement(child))
    return false;

  // Since the following items are not obligatory, we have to while()
  // until we have no more items...

  child = child.nextSiblingElement();

  while(!child.isNull())
    {
      if(child.tagName() == "prev-mnm-code")
        {
          if(prevSet)
            return false;
          else
            {
              m_prevCode = child.text();
              prevSet    = true;
            }
        }
      else if(child.tagName() == "curr-mnm-code")
        {
          if(currSet)
            return false;
          else
            {
              m_currCode = child.text();
              currSet    = true;
            }
        }
      else if(child.tagName() == "next-mnm-code")
        {
          if(nextSet)
            return false;
          else
            {
              m_nextCode = child.text();
              nextSet    = true;
            }
        }
      else if(child.tagName() == "comment")
        {
          if(commentSet)
            return false;
          else
            {
              m_comment  = child.text();
              commentSet = true;
            }
        }

      child = child.nextSiblingElement();
    }

  if(!validate())
    return false;

  return true;
}


//! Formats a string suitable to use as an XML element.
/*! Formats a string suitable to be used as an XML element in a
  polymer chemistry definition file. The typical fragmentation
  rule element that is generated in this function looks like
  this:

  \verbatim
  <fgr>
  <name>a-fgr-2</name>
  <formula>+H100</formula>
  <prev-mnm-code>F</prev-mnm-code>
  <curr-mnm-code>D</curr-mnm-code>
  <next-mnm-code>E</next-mnm-code>
  <comment>comment here!</comment>
  </fgr>
  \endverbatim

  \param offset times the \p indent string must be used as a lead in the
  formatting of elements.

  \param indent string used to create the leading space that is placed
  at the beginning of indented XML elements inside the XML
  element. Defaults to two spaces(QString(" ")).

  \return a dynamically allocated string that needs to be freed after
  use.

  \sa renderXmlFgrElement(const QDomElement &element).
*/
QString *
FragRule::formatXmlFgrElement(int offset, const QString &indent)
{

  int newOffset;
  int iter = 0;

  QString lead("");
  QString *string = new QString();


  // Prepare the lead.
  newOffset = offset;
  while(iter < newOffset)
    {
      lead += indent;
      ++iter;
    }

  *string += QString("%1<fgr>\n").arg(lead);

  // Prepare the lead.
  ++newOffset;
  lead.clear();
  iter = 0;
  while(iter < newOffset)
    {
      lead += indent;
      ++iter;
    }

  // Continue with indented elements.

  *string += QString("%1<name>%2</name>\n").arg(lead).arg(m_name);

  *string += QString("%1<formula>%2</formula>\n").arg(lead).arg(m_formula);

  if(!m_prevCode.isEmpty())
    *string += QString("%1<prev-mnm-code>%2</prev-mnm-code>\n")
                 .arg(lead)
                 .arg(m_prevCode);

  if(!m_currCode.isEmpty())
    *string += QString("%1<curr-mnm-code>%2</curr-mnm-code>\n")
                 .arg(lead)
                 .arg(m_currCode);

  if(!m_nextCode.isEmpty())
    *string += QString("%1<next-mnm-code>%2</next-mnm-code>\n")
                 .arg(lead)
                 .arg(m_nextCode);

  if(!m_comment.isEmpty())
    *string += QString("%1<comment>%2</comment>\n").arg(lead).arg(m_comment);

  // Prepare the lead.
  --newOffset;
  lead.clear();
  iter = 0;
  while(iter < newOffset)
    {
      lead += indent;
      ++iter;
    }

  *string += QString("%1</fgr>\n").arg(lead);


  return string;
}

} // namespace libmass

} // namespace msxps

