/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "MassPeakShaperConfig.hpp"


namespace msxps
{
namespace libmass
{


MassPeakShaperConfig::MassPeakShaperConfig()
{
  reset();
}


MassPeakShaperConfig::MassPeakShaperConfig(const MassPeakShaperConfig &other)
  : m_resolution(other.m_resolution),
    m_fwhm(other.m_fwhm),
    m_referencePeakMz(other.m_referencePeakMz),
    m_pointCount(other.m_pointCount),
    m_withBins(other.m_withBins),
    m_binSizeDivisor(other.m_binSizeDivisor),
    m_binSize(other.m_binSize),
    m_isBinSizeFixed(other.m_isBinSizeFixed),
    m_ionizationFormula(other.m_ionizationFormula),
    m_mzStep(other.m_mzStep),
    m_normFactor(other.m_normFactor),
    m_massPeakShapeType(other.m_massPeakShapeType)
{
}


MassPeakShaperConfig::~MassPeakShaperConfig()
{
}


void
MassPeakShaperConfig::operator=(const MassPeakShaperConfig &other)
{
  m_resolution        = other.m_resolution;
  m_fwhm              = other.m_fwhm;
  m_referencePeakMz   = other.m_referencePeakMz;
  m_pointCount        = other.m_pointCount;
  m_withBins          = other.m_withBins;
  m_binSizeDivisor    = other.m_binSizeDivisor;
  m_binSize           = other.m_binSize;
  m_isBinSizeFixed    = other.m_isBinSizeFixed;
  m_ionizationFormula = other.m_ionizationFormula;
  m_mzStep            = other.m_mzStep;
  m_normFactor        = other.m_normFactor;
  m_massPeakShapeType = other.m_massPeakShapeType;
}


void
MassPeakShaperConfig::setConfig(const MassPeakShaperConfig &other)
{
  *this = other;
}


void
MassPeakShaperConfig::setResolution(int value)
{
  m_resolution = value;
}


double
MassPeakShaperConfig::getResolution() const
{
  return m_resolution;
}


int
MassPeakShaperConfig::resolution(bool *ok)
{
  if(ok == nullptr)
    qFatal("The pointer cannot be nullptr.");

  // If we want to compute the resolution that means that we have to have
  // m_fwhm.
  if(!m_fwhm)
    {
      *ok = false;
      return 0;
    }

  if(!m_referencePeakMz)
    {
      *ok = false;
      return 0;
    }

  m_resolution = m_referencePeakMz / m_fwhm;

  *ok = true;

  // We used the FWHM to compute the resolving power.
  m_massPeakWidthLogic = MassPeakWidthLogic::FWHM;

  return m_resolution;
}


void
MassPeakShaperConfig::setFwhm(double value)
{
  m_fwhm = value;
}


double
MassPeakShaperConfig::getFwhm() const
{
  return m_fwhm;
}


double
MassPeakShaperConfig::fwhm(bool *ok)
{
  if(ok == nullptr)
    qFatal("The pointer cannot be nullptr.");

  // Or we need to compute it using the mz passed as parameter and the
  // resolution.
  if(!m_resolution)
    {
      *ok = false;
      return 0;
    }

  if(!m_referencePeakMz)
    {
      // qDebug() << "There is no reference peak centroid!";

      *ok = false;
      return 0;
    }

  m_fwhm = m_referencePeakMz / m_resolution;
  *ok    = true;

  // We used the resolving power to compute the FWHM.
  m_massPeakWidthLogic = MassPeakWidthLogic::RESOLUTION;

  return m_fwhm;
}


// For the lorentzian, that is half of the fwhm.
double
MassPeakShaperConfig::halfFwhm(bool *ok)
{
  double fwhm_value = fwhm(ok);

  if(!*ok)
    return 0;

  return (fwhm_value / 2);
}

void
MassPeakShaperConfig::setReferencePeakMz(double mz)
{
  m_referencePeakMz = mz;
}


double
MassPeakShaperConfig::getReferencePeakMz() const
{
  return m_referencePeakMz;
}


void
MassPeakShaperConfig::setIonizationFormula(const QString &ionization_formula)
{
  m_ionizationFormula = ionization_formula;
}


QString
MassPeakShaperConfig::getIonizationFormula() const
{
  return m_ionizationFormula;
}


void
MassPeakShaperConfig::setWithBins(bool with_bins)
{
  m_withBins = with_bins;
}


bool
MassPeakShaperConfig::withBins() const
{
  return m_withBins;
}


void
MassPeakShaperConfig::setBinSize(double bin_size)
{
  m_binSize = bin_size;
}


double
MassPeakShaperConfig::binSize(bool *ok)
{
  if(ok == nullptr)
    qFatal("The pointer cannot be nullptr.");

  // If the bin size was set before, then, just return it.
  // if(m_binSize)
  //{
  //*ok = true;
  // return m_binSize;
  //}

  if(!m_withBins)
    {
      // qDebug() << "Bins are not requested, just return 0 and set true.";
      *ok = true;
      return 0;
    }

  // In order to compute the bin Size, we need the FWHM and the number of
  // points.

  if(!m_resolution && !m_fwhm)
    {
      // qDebug() << "That's an error when neither resolution nor FWHM is set.";
      *ok = false;
      return 0;
    }

  if(m_fwhm)
    {
      // FWHM is fine, we can use that immediately.

      // qDebug() << "FWHM:" << m_fwhm;

      if(!m_pointCount)
        {
          // qDebug() << "That's an error that the point count is 0.";
          *ok = false;
          return 0;
        }
    }
  else
    {
      // We have to work with the resolution.

      // qDebug() << "Resolution:" << m_resolution;

      fwhm(ok);

      if(!*ok)
        {
          // qDebug()
          //<< "Could not compute FWHM on the basis of the resolution.";

          if(!m_pointCount)
            {
              // qDebug() << "That's an error that the point count is 0.";
              *ok = false;
              return 0;
            }
        }
    }

  if(m_isBinSizeFixed)
    {
      // The bin size has to be set and must not be changed, as the user has
      // set it manually.

      if(!m_binSize)
        {
          // qDebug() << "The bin size should be set manually but is not set.";
          *ok = false;
          return 0;
        }
    }
  else
    {
      m_binSize = m_fwhm / m_binSizeDivisor;

      // qDebug() << "The bin size was computed:" << m_binSize;
    }

  *ok = true;
  return m_binSize;
}


double
MassPeakShaperConfig::getBinSize() const
{
  return m_binSize;
}


void
MassPeakShaperConfig::setBinSizeFixed(bool is_fixed)
{
  m_isBinSizeFixed = is_fixed;
}


bool
MassPeakShaperConfig::getBinSizeFixed()
{
  return m_isBinSizeFixed;
}


void
MassPeakShaperConfig::setBinSizeDivisor(int factor)
{
  if(std::abs(factor) < 1)
    qFatal("Programming error.");

  m_binSizeDivisor = std::abs(factor);
}


int
MassPeakShaperConfig::getBinSizeDivisor() const
{
  return m_binSizeDivisor;
}


void
MassPeakShaperConfig::setPointCount(int value)
{
  m_pointCount = value;
}


int
MassPeakShaperConfig::getPointCount() const
{
  return m_pointCount;
}


void
MassPeakShaperConfig::setNormFactor(double value)
{
  m_normFactor = value;
}


double
MassPeakShaperConfig::normFactor()
{
  return m_normFactor;
}


void
MassPeakShaperConfig::setMassPeakShapeType(MassPeakShapeType value)
{
  m_massPeakShapeType = value;
}


MassPeakShapeType
MassPeakShaperConfig::getMassPeakShapeType() const
{
  return m_massPeakShapeType;
}


void
MassPeakShaperConfig::setMassPeakWidthLogic(MassPeakWidthLogic logic)
{
  m_massPeakWidthLogic = logic;
}


MassPeakWidthLogic
MassPeakShaperConfig::getMassPeakWidthLogic() const
{
  return m_massPeakWidthLogic;
}


double
MassPeakShaperConfig::c(bool *ok)
{
  // c in the Gaussian curve is related to the fwhm value:

  fwhm(ok);

  if(!*ok)
    {
      return 0;
    }

  double c = m_fwhm / (2 * sqrt(2 * log(2)));

  // qDebug() << "c:" << c;

  *ok = true;

  return c;
}


double
MassPeakShaperConfig::a(bool *ok)
{
  //  double pi = 3.1415926535897932384626433832795029;

  double c_value = c(ok);

  if(!*ok)
    {
      return 0;
    }

  double a = (1 / (c_value * sqrt(2 * M_PI)));

  // qDebug() << "a:" << a;

  *ok = true;

  return a;
}


double
MassPeakShaperConfig::gamma(bool *ok)
{
  fwhm(ok);

  if(!*ok)
    {
      return 0;
    }

  double gamma = m_fwhm / 2;

  // qDebug() << "gamma:" << gamma;

  *ok = true;

  return gamma;
}


void
MassPeakShaperConfig::setMzStep(double value)
{
  m_mzStep = value;
}


double
MassPeakShaperConfig::getMzStep() const
{
  return m_mzStep;
}


double
MassPeakShaperConfig::mzStep(bool *ok)
{
  // But what is the mz step ?
  //
  // We want the shape to be able to go down to baseline. Thus we want that
  // the shape to have a "basis" (or, better, a "ground") corresponding to
  // twice the FWHM on the left of the centroid and to twice the FWHM on the
  // right (that makes in total FWHM_PEAK_SPAN_FACTOR * FWHM, that is,
  // FWHM_PEAK_SPAN_FACTOR = 4).

  fwhm(ok);

  if(!*ok)
    {
      return 0;
    }

  if(!m_pointCount)
    {
      *ok = false;
      return 0;
    }

  m_mzStep = (FWHM_PEAK_SPAN_FACTOR * m_fwhm) / m_pointCount;

  return m_mzStep;
}


void
MassPeakShaperConfig::reset()
{
  // Values to start over.

  m_resolution        = 0;
  m_fwhm              = 0;
  m_referencePeakMz   = 0;
  m_pointCount        = 0;
  m_withBins          = false;
  m_binSizeDivisor    = 0;
  m_binSize           = 0;
  m_isBinSizeFixed    = false;
  m_ionizationFormula = "";
  m_mzStep            = 0;
  m_normFactor        = 0;
  m_massPeakShapeType = MassPeakShapeType::NOT_SET;
}


bool
MassPeakShaperConfig::resolve()
{
  // We need to try to set all the relevant parameters by calculation.

  bool ok = false;

  // These are the essential parameters:

  if(!m_referencePeakMz)
    return false;

  if(!m_fwhm && !m_resolution)
    return false;

  if(!m_pointCount)
    return false;

  // At this point we should be able to compute the relevant data.

  // The FWHM is the *leading* value for the determination of the peak shape's
  // width at half maximum. If that FWHM value is 0, then we resort to the
  // resolution. Both R and FWHM cannot be 0!
  if(m_fwhm)
    {
      // If we have FWHM, immediately try to compute the resolution as we'll
      // need it later. FWHM takes precedence over resolution!
      resolution(&ok);
      if(!ok)
        return false;
    }
  else
    {
      // We should be able to compute FWHM by resorting to the resolution.

      fwhm(&ok);

      if(!ok)
        return false;
    }

  // Now check if we have and can compute the bins.
  // But we do this only if the user has not stated that the bin size has to
  // be set manually.

  // qDebug() << "In the resolve, check the bin size";

  binSize(&ok);

  if(!ok)
    {
      return false;
    }

  mzStep(&ok);

  if(!ok)
    {
      return false;
    }

  // Now the other parameters for the shape.

  c(&ok);

  if(!ok)
    {
      return false;
    }

  a(&ok);

  if(!ok)
    {
      return false;
    }

  gamma(&ok);

  if(!ok)
    {
      return false;
    }

  return true;
}


QString
MassPeakShaperConfig::toString()
{
  QString string;

  bool ok = resolve();

  if(!ok)
    return QString();

  QString peak_shape_text;
  if(m_massPeakShapeType == MassPeakShapeType::GAUSSIAN)
    peak_shape_text = "Gaussian";
  if(m_massPeakShapeType == MassPeakShapeType::LORENTZIAN)
    peak_shape_text = "Lorentzian";

  QString with_bins_text;
  if(m_withBins)
    with_bins_text +=
      QString("With bins of size: %1 m/z.\n").arg(m_binSize, 0, 'f', 10);
  else
    with_bins_text = "Without bins.\n";

  string = QString(
             "%1 peak shaping:\n"
             "Configuration for reference m/z value: %2\n"
             "Resolution: %3\n"
             "FWHM: %4\n"
             "%5\n"
             "Number of points to shape the peak: %6\n"
             "c: %7\n"
             "c^2: %8\n"
             "mz step: %9\n\n")
             .arg(peak_shape_text)
             .arg(m_referencePeakMz, 0, 'f', 5)
             .arg(m_resolution)
             .arg(m_fwhm, 0, 'f', 5)
             .arg(with_bins_text)
             .arg(m_pointCount)
             .arg(c(&ok), 0, 'f', 5)
             .arg(c(&ok) * c(&ok), 0, 'f', 5)
             .arg(m_mzStep, 0, 'f', 5);

  return string;
}

} // namespace libmass

} // namespace msxps
