/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 * This specific code is a port to C++ of the Envemind Python code by Radzinski
 * and colleagues of IsoSpec fame (Lacki, Startek and company :-)
 *
 * See https://github.com/PiotrRadzinski/envemind.
 * *****************************************************************************
 *
 * END software license
 */


#include <QDebug>


#include <pappsomspp/trace/datapoint.h>


#include "Envemind.hpp"


namespace msxps
{
namespace libmass
{


Envemind::Envemind()
{
}


Envemind::Envemind(const pappso::Trace &trace) : m_trace(trace)
{
}


Envemind::Envemind(const Envemind &other)
{
  m_trace = other.m_trace;
}


Envemind::~Envemind()
{
}


Envemind &
Envemind::operator=(const Envemind &other)
{
  m_trace = other.m_trace;

  return *this;
}


double
Envemind::monoIsotopicMassPrediction()
{
  double mass = 0.0;

  return mass;
}


double
Envemind::getMostAbundant(bool *ok)
{
  if(ok == nullptr)
    qFatal("Pointer cannot be nullptr.");

  if(!m_trace.size())
  {
   *ok = false;
   return 0.0;
  }

  std::vector<pappso::DataPoint>::iterator iter =
    maxYDataPoint(m_trace.begin(), m_trace.end());

  if(iter == m_trace.end())
    qFatal("Failed to find the most intense data point.");

  return iter->x;
}


} // namespace libmass

} // namespace msxps
