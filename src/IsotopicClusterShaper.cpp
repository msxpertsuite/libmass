/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>
#include <algorithm>
#include <limits> // for std::numeric_limits


/////////////////////// Qt includes

/////////////////////// libmass includes
#include <libmass/globals.hpp>
#include <libmass/MassDataCborMassSpectrumHandler.hpp>

/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/processing/filters/filternormalizeintensities.h>

/////////////////////// Local includes
#include "IsotopicClusterShaper.hpp"
#include "MassPeakShaper.hpp"
#include "pappsomspp/utils.h"


namespace msxps
{

namespace libmass
{


IsotopicClusterShaper::IsotopicClusterShaper(
  const pappso::Trace &isotopic_cluster,
  int charge,
  const libmass::MassPeakShaperConfig &config)
  : m_config(config)
{
  // No need to reset, we are constructing.
  setIsotopicCluster(isotopic_cluster, charge, false);
}


IsotopicClusterShaper::IsotopicClusterShaper(
  const std::vector<IsotopicClusterChargePair> &isotopic_cluster_charge_pairs,
  const libmass::MassPeakShaperConfig &config)
  : m_config(config)
{
  // No need to reset, we are constructing.
  setIsotopicClusterChargePairs(isotopic_cluster_charge_pairs, false);
}


IsotopicClusterShaper::~IsotopicClusterShaper()
{
}


void
IsotopicClusterShaper::setConfig(libmass::MassPeakShaperConfig config)
{
  m_config = config;
}


libmass::MassPeakShaperConfig
IsotopicClusterShaper::getConfig() const
{
  return m_config;
}


void
IsotopicClusterShaper::setNormalizeIntensity(int new_max_intensity)
{
  m_normalizeIntensity = new_max_intensity;
}


int
IsotopicClusterShaper::getNormalizeIntensity() const
{
  return m_normalizeIntensity;
}


pappso::Trace &
IsotopicClusterShaper::run(bool reset)
{
  if(reset)
    {
      // Clear the map trace that will receive the results of the combinations.
      m_mapTrace.clear();
      m_finalTrace.clear();
    }

  if(!m_config.resolve())
    {
      qDebug() << "The peak shaper configuration failed to resolve.";
      return m_finalTrace;
    }

  // This class works on a vector of pairs containing the following:
  // 1. a pappso::TraceCstSPtr
  // 2. a charge

  // We will process each pair in turn. If the integration requires bin, then
  // each shaped isotopic cluster will be combined into a mass spectrum.
  // Otherwise a trace combiner will be used.

  // When setting the data (either by construction or using the set<> functions,
  // we had monitored the smallest and the greatest m/z value over the whole set
  // of the DataPoint objects in the isotopic clusters (centroid data). This is
  // because we need, in case binning is required, these values to craft the
  // bins.

  // We will need to perform combinations, positive combinations.
  // This mass spectrum combiner is in case we need binning.
  pappso::MassSpectrumPlusCombiner mass_spectrum_plus_combiner;

  // This trace combiner is in case do *not* need binning.
  pappso::TracePlusCombiner trace_plus_combiner(-1);

  // Configure the mass spectrum combiner in case we need binning.

  if(m_config.withBins())
    {
      // Bins were requested.

      //qDebug() << "Bins are required.";

      // Get the bin size out of the configuration.

      double bin_size = m_config.getBinSize();

      //qDebug() << "The bin size in the config is:" << bin_size;

      // Because we had monitored the m/z values of all the shapes generated
      // above, we know the smallest and greatest m/z value that were
      // encountered in all that peak shaping activity. We thus can create the
      // bins faithfully.

      MassPeakWidthLogic logic = m_config.getMassPeakWidthLogic();

      // The m_smallestMz and m_greatestMz values have been determined by
      // looking into the unshaped isotopic clusters passed to this object
      // either by construction or with functions. These two mz values are thus
      // peak centroids, not data points belonging to a shaped peak since we
      // have not yet started shaping the peaks. This means that we cannot
      // create bins start / ending at these values because we would loose the
      // first half of the first shaped peak centroid and the second half of the
      // last shaped peak centroid (a shaped peak goes left *and* right of the
      // peak centroid otherwise there would be no shape).
      //
      // This is why we provide a confortable margin fo the bin creation below
      // by removing 1 Th on the left of the smallest mz and by adding 1 Th on
      // right of the greatest mz.

      if(logic == MassPeakWidthLogic::FWHM)
        {
          m_mzIntegrationParams = pappso::MzIntegrationParams(
            m_smallestMz - 1,
            m_greatestMz + 1,
            pappso::BinningType::ARBITRARY,
            -1,
            pappso::PrecisionFactory::getDaltonInstance(bin_size),
            1,
            true);
        }
      else if(logic == MassPeakWidthLogic::RESOLUTION)
        {
          double resolution = m_config.getResolution();

          m_mzIntegrationParams = pappso::MzIntegrationParams(
            m_smallestMz - 1,
            m_greatestMz + 1,
            pappso::BinningType::ARBITRARY,
            -1,
            pappso::PrecisionFactory::getResInstance(resolution),
            m_config.getBinSizeDivisor(),
            true);
        }
      else
        qFatal(
          "Programming error. By this time, the mass peak width logic should "
          "have been defined.");

      //qDebug() << "The mz integration params:"
               //<< m_mzIntegrationParams.toString();

      // Now compute the bins.

      std::vector<double> bins = m_mzIntegrationParams.createBins();
      // qDebug() << "The bins:" << bins;

      mass_spectrum_plus_combiner.setBins(bins);
      // qDebug() << "Set bins to the mass spectrum combiner:"
      //<< mass_spectrum_plus_combiner.getBins().size();
    }

  std::size_t peak_centroid_count    = 0;
  std::size_t isotopic_cluster_count = 0;

  // Iterate in the isotopic cluster/charge pairs.
  for(auto isotopic_cluster_charge_pair : m_isotopicClusterChargePairs)
    {
      int charge = isotopic_cluster_charge_pair.second;

      // Iterate in the data points of the current centroid data isotopic
      // cluster.
      for(auto data_point : *isotopic_cluster_charge_pair.first)
        {
          // Note the division by m_charge below!

          pappso::Trace trace = MassPeakShaper::computePeakShape(
            data_point.x / charge, data_point.y, m_config);

          // qDebug() << "The shaped isotopic cluster has size:" <<
          // trace.size();

          if(trace.size())
            {
              if(m_config.withBins())
                mass_spectrum_plus_combiner.combine(m_mapTrace, trace);
              else
                trace_plus_combiner.combine(m_mapTrace, trace);

              //qDebug() << qSetRealNumberPrecision(15)
                       //<< "The map trace for combination has size:"
                       //<< m_mapTrace.size()
                       //<< "and starting m/z:" << m_mapTrace.begin()->first
                       //<< "and ending m/z:"
                       //<< std::prev(m_mapTrace.end())->first;

              ++peak_centroid_count;
            }
        }
      ++isotopic_cluster_count;
    }

  //qDebug() << QString(
                //"Successfully processed %1 isotopic clusters for a total of %2 "
                //"shaped peak centroids")
                //.arg(isotopic_cluster_count)
                //.arg(peak_centroid_count);

  // The user might have asked that the most intense m/z peak centroid be used
  // for normalization. In that case that peak centroid's intensity is brought
  // to m_normalizeIntensity and the ratio between its current intensity and
  // m_normalizeIntensity is used to normalize all the other data points in the
  // trace.

  if(m_normalizeIntensity != 1)
    {

      //qDebug() << "Now normalizing to  intensity = " << m_normalizeIntensity;

      pappso::Trace trace = m_mapTrace.toTrace();
      m_finalTrace =
        trace.filter(pappso::FilterNormalizeIntensities(m_normalizeIntensity));

      // double max_int = normalized_trace.maxYDataPoint().y;
      // qDebug() << "After normalization max int:" << max_int;
    }
  else
    m_finalTrace = m_mapTrace.toTrace();

  //qDebug() << "Returning a trace of size:" << m_finalTrace.size();

  //pappso::Utils::writeToFile(m_finalTrace.toString(), "/tmp/mass/trace.txt");

  return m_finalTrace;
}


// This is the workhorse of the handling of the input isotopic cluster. All the
// functions below end up here.
void
IsotopicClusterShaper::setIsotopicCluster(
  pappso::TraceCstSPtr isotopic_cluster_sp, int charge, bool reset)
{
  if(reset)
    m_isotopicClusterChargePairs.clear();

  double min_x = isotopic_cluster_sp->minX();
  m_smallestMz = std::min(m_smallestMz, min_x);

  double max_x = isotopic_cluster_sp->maxX();
  m_greatestMz = std::max(m_greatestMz, max_x);

  m_mostIntensePeakMz = isotopic_cluster_sp->maxYDataPoint().x;

  //qDebug() << qSetRealNumberPrecision(15) << "m_smallestMz:" << m_smallestMz
           //<< "m_greatestMz:" << m_greatestMz
           //<< "m_mostIntensePeakMz:" << m_mostIntensePeakMz;

  m_isotopicClusterChargePairs.push_back(
    IsotopicClusterChargePair(isotopic_cluster_sp, charge));

  m_config.setReferencePeakMz(m_mostIntensePeakMz);
}


void
IsotopicClusterShaper::setIsotopicCluster(
  pappso::TraceCstSPtr isotopic_cluster_sp, int charge)
{
  setIsotopicCluster(isotopic_cluster_sp, charge, true);
}


void
IsotopicClusterShaper::setIsotopicCluster(const pappso::Trace &isotopic_cluster,
                                          int charge,
                                          bool reset)
{
  setIsotopicCluster(
    std::make_shared<const pappso::Trace>(isotopic_cluster), charge, reset);
}


void
IsotopicClusterShaper::setIsotopicCluster(const pappso::Trace &isotopic_cluster,
                                          int charge)
{
  setIsotopicCluster(
    std::make_shared<const pappso::Trace>(isotopic_cluster), charge, true);
}


void
IsotopicClusterShaper::setIsotopicCluster(
  IsotopicClusterChargePair isotopic_cluster_charge_pair)
{
  setIsotopicCluster(isotopic_cluster_charge_pair.first,
                     isotopic_cluster_charge_pair.second,
                     true);
}


void
IsotopicClusterShaper::setIsotopicCluster(
  IsotopicClusterChargePair isotopic_cluster_charge_pair, bool reset)
{
  setIsotopicCluster(isotopic_cluster_charge_pair.first,
                     isotopic_cluster_charge_pair.second,
                     reset);
}


void
IsotopicClusterShaper::setIsotopicClusterChargePairs(
  const std::vector<IsotopicClusterChargePair> &isotopic_cluster_charge_pairs,
  bool reset)
{
  for(auto cluster_charge_pair : isotopic_cluster_charge_pairs)
    setIsotopicCluster(
      cluster_charge_pair.first, cluster_charge_pair.second, reset);

  //qDebug() << qSetRealNumberPrecision(15) << "m_smallestMz:" << m_smallestMz
           //<< "m_greatestMz:" << m_greatestMz
           //<< "m_mostIntensePeakMz:" << m_mostIntensePeakMz;
}


void
IsotopicClusterShaper::setIsotopicClusterChargePairs(
  const std::vector<IsotopicClusterChargePair> &isotopic_cluster_charge_pairs)
{
  setIsotopicClusterChargePairs(isotopic_cluster_charge_pairs, true);

  //qDebug() << qSetRealNumberPrecision(15) << "m_smallestMz:" << m_smallestMz
           //<< "m_greatestMz:" << m_greatestMz
           //<< "m_mostIntensePeakMz:" << m_mostIntensePeakMz;
}


void
IsotopicClusterShaper::appendIsotopicCluster(
  const pappso::Trace &isotopic_cluster, int charge)
{
  // Do not clear the isotopic clusters!

  setIsotopicCluster(isotopic_cluster, charge, false);
}


void
IsotopicClusterShaper::appendIsotopicClusterChargePairs(
  const std::vector<IsotopicClusterChargePair> &isotopic_cluster_charge_pairs)
{
  setIsotopicClusterChargePairs(isotopic_cluster_charge_pairs, false);

  //qDebug() << "m_smallestMz:" << m_smallestMz << "m_greatestMz:" << m_greatestMz
           //<< "m_mostIntensePeakMz:" << m_mostIntensePeakMz;
}


QString
IsotopicClusterShaper::shapeToString()
{
  return m_finalTrace.toString();
}

} // namespace libmass

} // namespace msxps
