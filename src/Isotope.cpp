/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2024 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QString>
#include <QDebug>

/////////////////////// Local includes
#include "Isotope.hpp"

namespace msxps
{

namespace libmass
{


// #include <libisospec++/isoSpec++.h>
//
// extern const int elem_table_ID[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int
// elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];

// This is the order of the columns in the gui TableView
// ID
// ELEMENT,
// SYMBOL,
// ATOMIC_NO,
// MASS,
// MASS_NO,
// EXTRA_NEUTRONS,
// PROBABILITY,
// LOG_PROBABILITY,
// RADIOACTIVE,

Isotope::Isotope(int id,
                 QString element,
                 QString symbol,
                 int atomicNo,
                 double mass,
                 int massNo,
                 int extraNeutrons,
                 double probability,
                 double logProbability,
                 bool radioactive)
  : m_id(id),
    m_element(element),
    m_symbol(symbol),
    m_atomicNo(atomicNo),
    m_mass(mass),
    m_massNo(massNo),
    m_extraNeutrons(extraNeutrons),
    m_probability(probability),
    m_logProbability(logProbability),
    m_radioactive(radioactive)
{
}


Isotope::Isotope(const Isotope &other)
{
  m_id             = other.m_id;
  m_element        = other.m_element;
  m_symbol         = other.m_symbol;
  m_atomicNo       = other.m_atomicNo;
  m_mass           = other.m_mass;
  m_massNo         = other.m_massNo;
  m_extraNeutrons  = other.m_extraNeutrons;
  m_probability    = other.m_probability;
  m_logProbability = other.m_logProbability;
  m_radioactive    = other.m_radioactive;
}


Isotope::Isotope(const QString &text)
{
  // This is somehow the reverse of toString().

  if(!initialize(text))
    qFatal("Failed to initialize an isotope with text.");
}


Isotope::~Isotope()
{
}


bool
Isotope::initialize(const QString &text)
{
  if(text.isEmpty())
    return false;

  // At this point deconstruct the line. Make sure we remove all spaces from
  // beginning and end.

  QString local_text = text.simplified();

  // At this point, use a regular expression to match the text.

  QStringList string_list = local_text.split(',');

  //qDebug() << "Expecting " << static_cast<int>(IsotopeFields::LAST)
           //<< "comma-separated fields for isotope-describing text line."
           //<< "QStringList is:" << string_list;

  if(string_list.size() != static_cast<int>(IsotopeFields::LAST))
    {
      qDebug() << "The text does not match an Isotope definition.";
      return false;
    }

  bool ok = false;

  m_id = string_list[static_cast<int>(IsotopeFields::ID)].toInt(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope ID.";
      return false;
    }

  m_element = string_list[static_cast<int>(IsotopeFields::ELEMENT)];

  if(m_element.isEmpty())
    {
      qDebug() << "Failed to extract the element name.";
      return false;
    }

  m_symbol = string_list[static_cast<int>(IsotopeFields::SYMBOL)];

  if(m_symbol.isEmpty())
    {
      qDebug() << "Failed to extract the element symbol.";
      return false;
    }

  m_atomicNo =
    string_list[static_cast<int>(IsotopeFields::ATOMIC_NUMBER)].toInt(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope atomic number.";
      return false;
    }

  m_mass = string_list[static_cast<int>(IsotopeFields::MASS)].toDouble(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope mass.";
      return false;
    }

  m_massNo =
    string_list[static_cast<int>(IsotopeFields::MASS_NUMBER)].toDouble(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope mass number.";
      return false;
    }

  m_extraNeutrons =
    string_list[static_cast<int>(IsotopeFields::EXTRA_NEUTRONS)].toInt(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope extra neutrons.";
      return false;
    }

  m_probability =
    string_list[static_cast<int>(IsotopeFields::PROBABILITY)].toDouble(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope probability.";
      return false;
    }

  m_logProbability =
    string_list[static_cast<int>(IsotopeFields::LOG_PROBABILITY)].toDouble(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope log probability.";
      return false;
    }

  m_radioactive =
    string_list[static_cast<int>(IsotopeFields::RADIOACTIVE)].toInt(&ok);

  if(!ok)
    {
      qDebug() << "Failed to extract the isotope radioactive.";
      return false;
    }

  return true;
}


void
Isotope::setId(int id)
{
  m_id = id;
}


int
Isotope::getId() const
{
  return m_id;
}


void
Isotope::setElement(const QString &element)
{
  m_element = element;
}


QString
Isotope::getElement() const
{
  return m_element;
}


void
Isotope::setSymbol(const QString &symbol)
{
  m_symbol = symbol;
}


QString
Isotope::getSymbol() const
{
  return m_symbol;
}


void
Isotope::setAtomicNo(int atomic_number)
{
  m_atomicNo = atomic_number;
}


int
Isotope::getAtomicNo() const
{
  return m_atomicNo;
}


void
Isotope::setMass(double mass)
{
  m_mass = mass;
}


double
Isotope::getMass() const
{
  return m_mass;
}


void
Isotope::setMassNo(int mass_number)
{
  m_massNo = mass_number;
}


int
Isotope::getMassNo() const
{
  return m_massNo;
}


void
Isotope::setExtraNeutrons(int extra_neutrons)
{
  m_extraNeutrons = extra_neutrons;
}


int
Isotope::getExtraNeutrons() const
{
  return m_extraNeutrons;
}


void
Isotope::setProbability(double probability)
{
  m_probability = probability;
}


double
Isotope::getProbability() const
{
  return m_probability;
}


void
Isotope::setLogProbability(double log_probability)
{
  m_logProbability = log_probability;
}


double
Isotope::getLogProbability() const
{
  return m_logProbability;
}


void
Isotope::setRadioactive(bool is_radioactive)
{
  m_radioactive = is_radioactive;
}


bool
Isotope::getRadioactive() const
{
  return m_radioactive;
}


int
Isotope::validate(QString *errors_p) const
{
  int error_count = 0;
  QString errors;

  // The minimal set of data that an isotope needs to have is the symbol, the
  // mass and the probability.

  if(m_symbol.isEmpty())
    {
      ++error_count;
      errors += "The symbol is not set.";
    }

  if(m_mass <= 0)
    {
      ++error_count;
      errors += "The mass is not set.";
    }

  if(m_probability > 1 || m_probability <= 0)
    {
      ++error_count;
      errors += "The probability is not set.";
    }

  // Now, if there were errors, we would like to document them with the symbol
  // name if it was set.

  if(error_count)
    {
      if(errors_p)
        {
          if(m_symbol.isEmpty())
            *errors_p = errors + "\n";
          else
            // Document the name of the symbol to make the errors more
            // meaningful.
            *errors_p += "For symbol " + m_symbol + ": " + errors + "\n";
        }
    }

  return error_count;
}


Isotope &
Isotope::operator=(const Isotope &other)
{
  if(&other == this)
    return *this;

  m_id             = other.m_id;
  m_element        = other.m_element;
  m_symbol         = other.m_symbol;
  m_atomicNo       = other.m_atomicNo;
  m_mass           = other.m_mass;
  m_massNo         = other.m_massNo;
  m_extraNeutrons  = other.m_extraNeutrons;
  m_probability    = other.m_probability;
  m_logProbability = other.m_logProbability;
  m_radioactive    = other.m_radioactive;

  return *this;
}

bool
Isotope::operator==(const Isotope &other) const
{
  int errors = 0;

  errors += (m_id == other.m_id ? 0 : 1);
  errors += (m_element == other.m_element ? 0 : 1);
  errors += (m_symbol == other.m_symbol ? 0 : 1);
  errors += (m_atomicNo == other.m_atomicNo ? 0 : 1);
  errors += (m_mass == other.m_mass ? 0 : 1);
  errors += (m_massNo == other.m_massNo ? 0 : 1);
  errors += (m_extraNeutrons == other.m_extraNeutrons ? 0 : 1);
  errors += (m_probability == other.m_probability ? 0 : 1);
  errors += (m_logProbability == other.m_logProbability ? 0 : 1);
  errors += (m_radioactive == other.m_radioactive ? 0 : 1);

  return (errors > 0 ? false : true);
}


bool
Isotope::operator!=(const Isotope &other) const
{
  return !(*this == other);
}


QString
Isotope::toString() const
{
  // We need to use CSV because there might be spaces in the
  // text in the IsoSpec tables.
  return QString("%1,%2,%3,%4,%5,%6,%7,%8,%9,%10")
    .arg(m_id)
    .arg(m_element)
    .arg(m_symbol)
    .arg(m_atomicNo)
    .arg(m_mass, 0, 'f', 60)
    .arg(m_massNo)
    .arg(m_extraNeutrons)
    .arg(m_probability, 0, 'f', 60)
    .arg(m_logProbability, 0, 'f', 60)
    .arg(m_radioactive ? 1 : 0);
}

} // namespace libmass

} // namespace msxps
