/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// IsoSpec
#include <IsoSpec++/isoSpec++.h>
#include <IsoSpec++/element_tables.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "libmass/PeakCentroid.hpp"
#include "IsotopicDataBaseHandler.hpp"


namespace msxps
{

namespace libmass
{


IsotopicDataBaseHandler::IsotopicDataBaseHandler(const QString &file_name)
  : m_fileName(file_name)
{
  // We cannot allow to have a nullptr msp_isotopicData member.
  msp_isotopicData = std::make_shared<IsotopicData>();
}


IsotopicDataBaseHandler::IsotopicDataBaseHandler(
  IsotopicDataSPtr isotopic_data_sp, const QString &file_name)
  : msp_isotopicData(isotopic_data_sp), m_fileName(file_name)
{
  // We cannot allow to have a nullptr msp_isotopicData member.
  if(msp_isotopicData == nullptr)
    msp_isotopicData = std::make_shared<IsotopicData>();
}


IsotopicDataBaseHandler::~IsotopicDataBaseHandler()
{
  // qDebug();
}


std::size_t
IsotopicDataBaseHandler::loadData([[maybe_unused]] const QString &file_name)
{
  qDebug() << "The base class does not load data.";
  return 0;
}

std::size_t
IsotopicDataBaseHandler::writeData([[maybe_unused]] const QString &file_name)
{
  qDebug() << "The base class does not write data.";
  return 0;
}


void
IsotopicDataBaseHandler::setIsotopicData(IsotopicDataSPtr isotopic_data_sp)
{
  msp_isotopicData = isotopic_data_sp;
}


IsotopicDataSPtr
IsotopicDataBaseHandler::getIsotopicData()
{
  return msp_isotopicData;
}


void
IsotopicDataBaseHandler::setFileName(const QString &file_name)
{
  m_fileName = file_name;
}


QString
IsotopicDataBaseHandler::getFileName()
{
  return m_fileName;
}


std::size_t
IsotopicDataBaseHandler::checkConsistency()
{
  qDebug() << "The base class does not check consistenty.";
  return 0;
}

} // namespace libmass

} // namespace msxps


#if 0

Example from IsoSpec.

For water H2O

const int elementNumber = 2;
const int isotopeNumbers[2] = {2,3};

const int atomCounts[2] = {2,1};


const double hydrogen_masses[2] = {1.00782503207, 2.0141017778};
const double oxygen_masses[3] = {15.99491461956, 16.99913170, 17.9991610};

const double* isotope_masses[2] = {hydrogen_masses, oxygen_masses};

const double hydrogen_probs[2] = {0.5, 0.5};
const double oxygen_probs[3] = {0.5, 0.3, 0.2};

const double* probs[2] = {hydrogen_probs, oxygen_probs};

IsoLayeredGenerator iso(Iso(elementNumber, isotopeNumbers, atomCounts, isotope_masses, probs), 0.99);

#endif
