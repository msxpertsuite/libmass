/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>
#include <iostream>
#include <iomanip>
#include <memory>


/////////////////////// Qt includes
#include <QDebug>
#include <QFile>


/////////////////////// pappsomspp includes
#include <pappsomspp/utils.h>


/////////////////////// Local includes
#include "MassPeakShaper.hpp"


namespace msxps
{
namespace libmass
{


MassPeakShaper::MassPeakShaper() : m_peakCentroid(0, 0)
{
  m_config.reset();
}


MassPeakShaper::MassPeakShaper(double mz,
                               double intensity,
                               const MassPeakShaperConfig &config)
  : m_peakCentroid(mz, intensity), m_config(config)
{
}


MassPeakShaper::MassPeakShaper(const pappso::DataPoint &data_point,
                               const MassPeakShaperConfig &config)
  : m_peakCentroid(data_point), m_config(config)
{
  // qDebug()"m_config:" << m_config.asText(800);
}


MassPeakShaper::MassPeakShaper(const MassPeakShaper &other)
  : m_peakCentroid(other.m_peakCentroid),
    m_config(other.m_config),
    m_trace(other.m_trace)
{
}


MassPeakShaper::~MassPeakShaper()
{
}

void
MassPeakShaper::setPeakCentroid(const pappso::DataPoint &data_point)
{
  m_peakCentroid = data_point;
}


const pappso::DataPoint &
MassPeakShaper::getPeakCentroid() const
{
  return m_peakCentroid;
}


const pappso::Trace &
MassPeakShaper::getTrace() const
{
  return m_trace;
}


void
MassPeakShaper::clearTrace()
{
  m_trace.clear();
}


void
MassPeakShaper::setConfig(const MassPeakShaperConfig &config)
{
  m_config.setConfig(config);
}


const MassPeakShaperConfig &
MassPeakShaper::getConfig() const
{
  return m_config;
}


int
MassPeakShaper::computePeakShape()
{
  if(m_config.getMassPeakShapeType() == MassPeakShapeType::GAUSSIAN)
    return computeGaussianPeakShape();
  else
    return computeLorentzianPeakShape();
}


pappso::Trace
MassPeakShaper::computePeakShape(double mz,
                                 double intensity,
                                 const MassPeakShaperConfig &config)
{
  if(config.getMassPeakShapeType() == MassPeakShapeType::GAUSSIAN)
    return computeGaussianPeakShape(mz, intensity, config);
  else
    return computeLorentzianPeakShape(mz, intensity, config);
}


int
MassPeakShaper::computeGaussianPeakShape()
{
  // qDebug();

  m_trace.clear();

  m_trace =
    computeGaussianPeakShape(m_peakCentroid.x, m_peakCentroid.y, m_config);

  return m_trace.size();
}


// Static version
pappso::Trace
MassPeakShaper::computeGaussianPeakShape(double mz,
                                         double intensity,
                                         const MassPeakShaperConfig &config)
{
  pappso::Trace trace;

  // We will use the data in the configuration object. First check that
  // we can rely on it. This call sets all the proper values to the m_config's
  // member data after having validate each.

  MassPeakShaperConfig local_config = config;

  if(!local_config.resolve())
    {
      qDebug() << "Failed to resolve the MassPeakShaperConfig.";
      return trace;
    }

  // qDebug() << "The peak shaper configuration:" << m_config.toString();

  // First off, we need to tell what the height of the gaussian peak should
  // be.
  double a;
  // a = m_config.a(mz);

  // We actually set a to 1, because it is the intensity above that will
  // provide the height of the peak, see below where the heigh of the peak is
  // set to a * intensity, that is, intensity if a = 1.
  a = 1;

  // qDebug() << "a:" << a;

  bool ok = false;

  double c = local_config.c(&ok);

  if(!ok)
    {
      return trace;
    }

  double c_square = c * c;

  // qDebug() << "c:" << c << "c²:" << c_square;


  // Were are the left and right points of the shape ? We have to
  // determine that using the point count and mz step values.

  // Compute the mz step that will separate two consecutive points of the
  // shape. This mzStep is function of the number of points we want for a
  // given peak shape and the width of the peak shape left and right of the
  // centroid.

  double mz_step = local_config.getMzStep();

  double left_point =
    mz - ((double)FWHM_PEAK_SPAN_FACTOR / 2 * local_config.getFwhm());
  double right_point =
    mz + ((double)FWHM_PEAK_SPAN_FACTOR / 2 * local_config.getFwhm());

  // qDebug() << "left m/z:" << left_point;
  // qDebug() << "right m/z:" << right_point;

  int iterations = (right_point - left_point) / mz_step;
  double x       = left_point;

  for(int iter = 0; iter < iterations; ++iter)
    {
      double y = intensity * a * exp(-1 * (pow((x - mz), 2) / (2 * c_square)));

      trace.push_back(pappso::DataPoint(x, y));

      x += mz_step;
    }

  //qDebug() << qSetRealNumberPrecision(15) << "For centroid" << mz
           //<< "first shape point:" << left_point
           //<< "with trace:" << trace.toString();

  return trace;
}


int
MassPeakShaper::computeLorentzianPeakShape()
{
  // qDebug();

  m_trace.clear();

  m_trace =
    computeLorentzianPeakShape(m_peakCentroid.x, m_peakCentroid.y, m_config);

  return m_trace.size();
}


// Static version
pappso::Trace
MassPeakShaper::computeLorentzianPeakShape(double mz,
                                           double intensity,
                                           const MassPeakShaperConfig &config)
{
  pappso::Trace trace;

  // We will use the data in the configuration object. First check that
  // we can rely on it. This call sets all the proper values to the m_config's
  // member data after having validate each.

  MassPeakShaperConfig local_config = config;

  if(!local_config.resolve())
    {
      qDebug() << "Failed to resolve the MassPeakShaperConfig.";
      return trace;
    }

  // qDebug() << "The peak shaper configuration:" << m_config.toString();

  // First off, we need to tell what the height of the gaussian peak should
  // be.
  double a;
  // a = local_config.a(mz);

  // We actually set a to 1, because it is the intensity above that will
  // provide the height of the peak, see below where the heigh of the peak is
  // set to a * intensity, that is, intensity if a = 1.
  a = 1;

  // qDebug() << "a value:" << a;

  bool ok = false;

  // The calls below will trigger the computation of fwhm, if it is
  // equal to 0 because it was not set manually.
  double gamma = local_config.gamma(&ok);

  if(!ok)
    {
      return trace;
    }

  double gamma_square = gamma * gamma;

  // qDebug() << "gamma:" << gamma << "gamma²:" << gamma_square;

  // Were are the left and right points of the shape ? We have to
  // determine that using the m_points and m_increment values.

  // Compute the mz step that will separate two consecutive points of the
  // shape. This mzStep is function of the number of points we want for a
  // given peak shape and the width of the peak shape left and right of the
  // centroid.

  double mz_step = local_config.getMzStep();

  double left_point =
    mz - ((double)FWHM_PEAK_SPAN_FACTOR / 2 * local_config.getFwhm());
  double right_point =
    mz + ((double)FWHM_PEAK_SPAN_FACTOR / 2 * local_config.getFwhm());

  // qDebug() << "left m/z:" << left_point;
  // qDebug() << "right m/z:" << right_point;

  int iterations = (right_point - left_point) / mz_step;
  double x       = left_point;

  for(int iter = 0; iter < iterations; ++iter)
    {
      double y =
        intensity * a * (gamma_square / (pow((x - mz), 2) + gamma_square));

      trace.push_back(pappso::DataPoint(x, y));

      x += mz_step;
    }

  // qDebug().noquote() << m_trace.toString();

  return trace;
}


double
MassPeakShaper::intensityAt(double mz,
                            pappso::PrecisionPtr precision_p,
                            bool &ok)
{
  pappso::DataPoint data_point = m_trace.containsX(mz, precision_p);

  if(data_point.isValid())
    {
      ok = true;
      return data_point.y;
    }

  ok = false;
  return 0;
}


QString
MassPeakShaper::shapetoString()
{
  return m_trace.toString();
}


bool
MassPeakShaper::shapeToFile(const QString &file_name)
{
  return pappso::Utils::writeToFile(m_trace.toString(), file_name);
}


} // namespace libmass

} // namespace msxps
