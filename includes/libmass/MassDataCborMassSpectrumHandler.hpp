/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Std lib includes


/////////////////////// Qt includes
#include <QDebug>
#include <QCborStreamReader>
#include <QCborStreamWriter>


/////////////////////// PAPPSO includes
#include <pappsomspp/trace/datapoint.h>
#include <pappsomspp/trace/trace.h>

/////////////////////// Local includes
#include "globals.hpp"
#include "MassDataCborBaseHandler.hpp"


namespace msxps
{

namespace libmass
{

// enum class MassDataType
//{
// UNSET = 0,
// MASS_SPECTRUM,
// DRIFT_SPECTRUM,
// TIC_CHROMATOGRAM,
// XIC_CHROMATOGRAM,
// MZ_RT_COLORMAP,
// DT_RT_COLORMAP,
// DT_MZ_COLORMAP,
//};


class MassDataCborMassSpectrumHandler : public MassDataCborBaseHandler
{
  Q_OBJECT

  public:
  MassDataCborMassSpectrumHandler(QObject *parent_p);
  MassDataCborMassSpectrumHandler(QObject *parent_p,
                           const pappso::Trace &trace);
  ~MassDataCborMassSpectrumHandler();

  virtual bool readFile(const QString &input_file_name = QString()) override;
  virtual bool readByteArray(const QByteArray &byte_array) override;
  virtual bool writeFile(const QString &output_file_name = QString()) override;
  virtual void writeByteArray(QByteArray &byte_array) override;

  void setXLabel(const QString &label);
  QString getXLabel() const;

  void setYLabel(const QString &label);
  QString getYLabel() const;

  void setTrace(const pappso::Trace &trace);
  pappso::Trace getTrace() const;
  void clearTrace();

  void setTraceColor(const QByteArray &color_byte_array);
  QByteArray getTraceColor() const;

  protected:
  pappso::Trace m_trace;

  QByteArray m_colorByteArray;

  QByteArray m_xBase64Data;
  QByteArray m_yBase64Data;

  QString m_xLabel;
  QString m_yLabel;

  virtual bool readContext(QCborStreamReaderSPtr &reader_sp) override;
};

typedef std::shared_ptr<MassDataCborMassSpectrumHandler> MassDataCborMassSpectrumHandlerSPtr;

} // namespace libmass

} // namespace msxps

