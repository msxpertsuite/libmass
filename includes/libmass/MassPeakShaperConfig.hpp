/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "globals.hpp"
#include "PeakCentroid.hpp"


namespace msxps
{
namespace libmass
{


enum class MassPeakShapeType
{
  NOT_SET = 0,
  GAUSSIAN,
  LORENTZIAN,
};

enum class MassPeakWidthLogic
{
  NOT_SET = 0,
  FWHM,
  RESOLUTION
};

// The MassPeakShaperConfig class contains all required data to configure the
// computation that will form a gaussian or lorentzian shape corresponding to
// a given centroid DataPoint (x,y) with x=m/z and y=relative intensity.
class MassPeakShaperConfig
{
  public:
  MassPeakShaperConfig();
  MassPeakShaperConfig(const MassPeakShaperConfig &other);

  virtual ~MassPeakShaperConfig();

  void operator=(const MassPeakShaperConfig &other);

  void setConfig(const MassPeakShaperConfig &other);

  void setResolution(int resolution);
  double getResolution() const;
  int resolution(bool *ok);

  // For the gaussion, that is the entirety of the fwhm.
  void setFwhm(double value);
  double getFwhm() const;
  double fwhm(bool *ok);
  // For the lorentzian, that is half of the fwhm.
  double halfFwhm(bool *ok);

  void setReferencePeakMz(double mz);
  double getReferencePeakMz() const;

  void setIonizationFormula(const QString &ionization_formula);
  QString getIonizationFormula() const;

  void setBinSize(double bin_size);
  double binSize(bool *ok);
  double getBinSize() const;
  void setWithBins(bool with_bins);
  bool withBins() const;
  void setBinSizeFixed(bool is_fixed);
  bool getBinSizeFixed();

  void setBinSizeDivisor(int divisor);
  int getBinSizeDivisor() const;

  void setPointCount(int);
  int getPointCount() const;

  void setNormFactor(double);
  double normFactor();

  MassPeakShapeType getMassPeakShapeType() const;
  void setMassPeakShapeType(MassPeakShapeType);

  void setMassPeakWidthLogic(MassPeakWidthLogic logic);
  MassPeakWidthLogic getMassPeakWidthLogic() const;

  double c(bool *ok);

  double a(bool *ok);

  double gamma(bool *ok);

  void setMzStep(double step);
  double getMzStep() const;
  double mzStep(bool *ok);

  bool resolve();
  void reset();

  QString toString();

  private:
  int m_resolution;
  double m_fwhm;

  MassPeakWidthLogic m_massPeakWidthLogic = MassPeakWidthLogic::NOT_SET;

  // The reference m/z value is the m/z value of the peak that is considered
  // to be the main peak in an isotopic cluster, for example. That is
  // typically the peak that is the most intense. This peak's intensity is
  // typically used to compute the FWHM on the basis of the resolution, for
  // example (see function fwhm().  When computing a Gaussian shape for a
  // single peak, then that reference peak's m/z value needs to be set as the
  // peak centroid for which the computation is performed.
  double m_referencePeakMz;

  // Number of points to use to shape the peak
  int m_pointCount;
  bool m_withBins;
  int m_binSizeDivisor;
  double m_binSize;
  bool m_isBinSizeFixed;
  QString m_ionizationFormula;
  int m_charge;
  // The delta bewteen two consecutive data points
  double m_mzStep;
  // Norm factor
  double m_normFactor;
  // Type (GAUSSIAN | LORENTZIAN) of the peak shape
  MassPeakShapeType m_massPeakShapeType;
};

} // namespace libmass

} // namespace msxps
