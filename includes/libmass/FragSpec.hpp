/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef FRAG_SPEC_HPP
#define FRAG_SPEC_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "FragRule.hpp"


namespace msxps
{


namespace libmass
{


//! Fragmentation end.
enum FragEnd
{
  FRAG_END_NONE  = 1 << 0,
  FRAG_END_LEFT  = 1 << 1,
  FRAG_END_RIGHT = 1 << 2,
  FRAG_END_BOTH  = (FRAG_END_LEFT | FRAG_END_RIGHT),
};


//! The FragSpec class provides a fragmentation specification.
/*! Fragmentation specifications determine the chemical reaction that
  governs the fragmentation of the polymer in the gas-phase. The
  chemical reaction is embodied by a formula. The side of the polymer
 (left or right) that makes the fragment after the fragmentation has
  occurred is described by a fragmentation-end enumeration.

  A fragmentation specification might not be enough information to
  determine the manner in which a polymer fragments in the
  gas-phase. Fragmentation rules might be required to refine the
  specification.  A fragmentation specification might hold as many
  fragmentation rules as required.
*/
class FragSpec : public PolChemDefEntity, public Formula
{
  public:
  FragSpec(PolChemDefCstSPtr,
           QString,
           QString         = QString(),
           FragEnd         = FRAG_END_NONE,
           const QString & = QString());

  FragSpec(PolChemDefCstSPtr, QString, QString);

  FragSpec(const FragSpec &other);

  ~FragSpec();

  using PolChemDefEntity::operator=;
  using Formula::operator=;
  FragSpec &operator=(const FragSpec &other);

  QList<FragRule *> &ruleList();
  void appendRule(FragRule *);
  void insertRuleAt(int, FragRule *);
  void removeRuleAt(int);

  void setFragEnd(FragEnd);
  FragEnd fragEnd() const;

  void setMonomerContribution(int);
  int monomerContribution();

  QString formula() const;

  void setComment(const QString &);
  QString comment() const;

  static int
  isNameInList(const QString &, const QList<FragSpec *> &, FragSpec * = 0);

using PolChemDefEntity::validate;
using Formula::validate;
  bool validate();

  bool renderXmlFgsElement(const QDomElement &element, int);

  QString *formatXmlFgsElement(int, const QString & = QString("  "));

  private:
  //! Fragmentation end.
  FragEnd m_fragEnd;
  //! Fragmented monomer's mass contribution. See fragSpecDefDlg.cpp
  //! for a detailed explanation of what this member is for.
  int m_monomerContribution;

  //! Comment.
  QString m_comment;

  //! List of fragmentation rules.
  QList<FragRule *> m_ruleList;
};

} // namespace libmass

} // namespace msxps


#endif // FRAG_SPEC_HPP
