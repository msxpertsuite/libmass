/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <limits>
#include <vector>
#include <memory>

/////////////////////// Qt includes


/////////////////////// Local includes
#include "globals.hpp"
#include "Isotope.hpp"


namespace msxps
{
namespace libmass
{

using IsotopeCstIterator = std::vector<IsotopeSPtr>::const_iterator;
using IsotopeIterator    = std::vector<IsotopeSPtr>::iterator;
using IsotopeCstIteratorPair =
  std::pair<IsotopeCstIterator, IsotopeCstIterator>;


class IsotopicData
{
  friend class IsotopicDataBaseHandler;
  friend class IsotopicDataLibraryHandler;
  friend class IsotopicDataUserConfigHandler;
  friend class IsotopicDataManualConfigHandler;

  public:
  IsotopicData();
  IsotopicData(const IsotopicData &other);
  virtual ~IsotopicData();

  void appendNewIsotope(IsotopeSPtr isotope_sp, bool update_maps = true);
  bool insertNewIsotope(IsotopeSPtr isotope_sp,
                        std::size_t index,
                        bool update_maps = true);

  void appendNewIsotopes(const std::vector<IsotopeSPtr> &isotopes,
                         bool update_maps = true);

  std::vector<IsotopeSPtr>::const_iterator eraseIsotopes(
    std::size_t begin_index, std::size_t end_index, bool update_maps);


  /////////////////////////////////////////////////////////
  /// Function to be run each time something is loaded or anything is changed
  /// int the m_isotopes vector.

  bool updateMonoMassMap(const QString &symbol);
  std::size_t updateMonoMassMap();

  bool updateAvgMassMap(const QString &symbol);
  std::size_t updateAvgMassMap();

  double computeAvgMass(IsotopeCstIteratorPair iter_pair,
                        std::vector<QString> &errors) const;

  void updateMassMaps(const QString &symbol);
  std::size_t updateMassMaps();

  /////////////////////////////////////////////////////////
  /// All the functions needed to access the various bits from the m_isotopes
  /// vector of IsotopeSPtr instances.

  // Get the monoisotopic mass of carbon.
  double getMonoMassBySymbol(const QString &symbol, bool *ok = nullptr) const;
  double getMonoMass(IsotopeCstIteratorPair iter_pair, std::vector<QString> &errors) const;

  double getAvgMassBySymbol(const QString &symbol, bool *ok = nullptr) const;

  double getCumulatedProbabilitiesBySymbol(const QString &symbol,
                                           std::vector<QString> &errors) const;

  double getCumulatedProbabilities(IsotopeCstIteratorPair iter_pair,
                                           std::vector<QString> &errors) const;

  IsotopeCstIteratorPair getIsotopesBySymbol(const QString &symbol) const;

  std::size_t getIsotopeCountBySymbol(const QString &symbol) const;

  IsotopeCstIteratorPair getIsotopesByName(const QString &name) const;

  std::vector<QString> getUniqueSymbolsInOriginalOrder() const;

  bool containsSymbol(const QString &symbol, int *count = nullptr) const;
  bool containsName(const QString &name, int *count = nullptr) const;

  QString isotopesAsStringBySymbol(const QString &symbol) const;

  bool isMonoMassIsotope(IsotopeCstSPtr isotope_csp);

  const std::vector<IsotopeSPtr> &getIsotopes() const;

  IsotopicData &operator=(const IsotopicData &other);

  // The number of isotopes in m_isotopes.
  std::size_t size() const;

  // The number of different symbols (that is chemical elements) in the data
  std::size_t getUniqueSymbolsCount() const;

  int validate(std::vector<QString> &errors) const;
  bool validateBySymbol(const QString &symbol, std::vector<QString> &errors) const;
  bool validateAllBySymbol(std::vector<QString> &errors) const;

  void replace(IsotopeSPtr old_isotope_sp, IsotopeSPtr new_isotope_sp);

  void clear();

  protected:
  // The vector should never be sorted as we want to keep the order of the
  // isotopes in the way the vector has been populated, either by looking into
  // the IsoSpec library tables or by reading data from a user-configured file.

  using IsotopeVectorCstIterator = std::vector<IsotopeSPtr>::const_iterator;
  std::vector<IsotopeSPtr> m_isotopes;

  std::map<QString, double> m_symbolMonoMassMap;
  std::map<QString, double> m_symbolAvgMassMap;
};

typedef std::shared_ptr<IsotopicData> IsotopicDataSPtr;
typedef std::shared_ptr<const IsotopicData> IsotopicDataCstSPtr;


IsotopicDataSPtr loadIsotopicDataFromFile(const QString &file_name,
                                          QString &error);
} // namespace libmass

} // namespace msxps

