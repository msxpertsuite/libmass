/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef PEAK_CENTROID_HPP
#define PEAK_CENTROID_HPP

#include <memory>

/////////////////////// Qt includes
#include <QtGlobal>
#include <QtXml>


/////////////////////// Local includes


namespace msxps
{

namespace libmass
{

  class PeakCentroid;

  typedef std::shared_ptr<PeakCentroid> PeakCentroidSPtr;

  class PeakCentroid
  {
    public:
    PeakCentroid();
    PeakCentroid(double /* mz */,
                 double /* intensity */);
    PeakCentroid(const PeakCentroid &);

    ~PeakCentroid();

    PeakCentroid &operator=(const PeakCentroid &other);
    bool operator==(const PeakCentroid &other);
    bool operator!=(const PeakCentroid &other);

    void setMz(double);
    double mz() const;

    void setIntensity(double);
    void incrementIntensity(double);
    double intensity() const;
    QString intensity(int decimalPlaces) const;

    QString toString() const;

    protected:
    double m_mz;
    double m_intensity;
  };

} // namespace libmass

} // namespace msxps


#endif // PEAK_CENTROID_HPP
