/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "globals.hpp"
#include "globals.hpp"
#include "Coordinates.hpp"

namespace msxps
{

namespace libmass
{


  enum SelectionType
  {
    SELECTION_TYPE_RESIDUAL_CHAINS,
    SELECTION_TYPE_OLIGOMERS,
  };

  //! The CalcOptions class provides calculation options.
  /*! Calculations can be performed in two manners :
   *
   *    - by taking into account the whole sequence;
   *
   *    - by taking into account the selected region of the sequence;
   *
   *    In the first case the computation is for the \em polymer, while in
   *    the second case the computation is for an \em oligomer.
   *
   *    Options involve also the way the computations are taking into
   *    account the left/end caps of the sequence or the monomer
   *    modifications, or the polymer modifications...
   */
  class CalcOptions
  {
    private:
    //! Tells if the calculation should involve the recalculation of
    /*! all the masses of the monomers of the sequence.
     */
    bool m_deepCalculation;

    CoordinateList m_coordinateList;

    //! MASS_TYPE.
    int m_massType;

    //! CAP_TYPE.
    int m_capping;

    //! MONOMER_CHEMENT.
    int m_monomerEntities;

    //! POLYMER_CHEMENT.
    int m_polymerEntities;

    SelectionType m_selectionType;

    public:
    CalcOptions();
    CalcOptions(bool,
                int = MassType::MASS_BOTH,
                int = CAP_BOTH,
                int = MONOMER_CHEMENT_NONE,
                int = POLYMER_CHEMENT_NONE);

    CalcOptions(const CalcOptions &other);

    ~CalcOptions();

    CalcOptions &operator=(const CalcOptions &other);

    void setDeepCalculation(bool);
    bool isDeepCalculation() const;

    void setStartIndex(int);
    int startIndex() const;

    void setEndIndex(int);
    int endIndex() const;

    void setCoordinateList(const Coordinates & = Coordinates());
    void setCoordinateList(const CoordinateList &);
    const CoordinateList &coordinateList() const;

    void setMassType(int);
    int massType() const;

    void setSelectionType(SelectionType);
    SelectionType selectionType() const;

    void setCapping(int);
    int capping() const;

    void setMonomerEntities(int);
    int monomerEntities() const;

    void setPolymerEntities(int);
    int polymerEntities() const;

    void debugPutStdErr() const;
  };


} // namespace libmass

} // namespace msxps
